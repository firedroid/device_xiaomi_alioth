
# Device Common Tree
echo 'Cloning Devie Common tree [1/4]'
git clone git@gitlab.com:firedroid/device_xiaomi_sm8250-common.git -b 14x device/xiaomi/sm8250-common

# Vendor Tree
echo 'Cloning Vendor tree [2/4]'
git clone git@gitlab.com:firedroid/vendor_xiaomi_alioth.git -b 14x vendor/xiaomi/alioth

# Vendor Common Tree
echo 'Cloning Vendor Common tree [3/4]'
git clone git@gitlab.com:firedroid/vendor_xiaomi_sm8250-common.git -b 14x vendor/xiaomi/sm8250-common

# Leica Cam Repo
echo 'Cloning leica Cam Repo [4/4]'
git clone git@gitlab.com:firedroid/vendor_xiaomi_camera.git -b stable vendor/xiaomi/camera